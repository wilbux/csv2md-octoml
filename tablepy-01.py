import pandas as pd
import datetime
import time
date_string = time.strftime("%Y-%m-%d-%H:%M:%S")
df = pd.read_csv("rn-9.0.csv")

with open("RelNotes_9-0.md", 'w') as md:
  df.to_markdown(buf=md, tablefmt="grid")
