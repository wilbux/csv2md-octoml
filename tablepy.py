import pandas as pd

df = pd.read_csv("rn-9.0.csv")
with open("releaseNotes-wPR.md", 'w') as md:
  df.to_markdown(buf=md, tablefmt="grid")
